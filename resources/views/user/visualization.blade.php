@extends('layouts.master')
@section('content')
<div class="container">
	<div
		class="col-md-12 col-lg-10 col-sm-12 col-xs-12 uac_container col-lg-offset-1">
		  <h1 class="border-title">Visualize</h1>
			<hr>
			@if ((Session::has('success-message')))
		<div class="alert alert-info">{{ Session::get('success-message') }}</div>
		@endif
		@if ((Session::has('logError-message')))
		<div class="alert alert-warning">{{ Session::get('logError-message') }}</div>
		@endif
		<div class="col-md-4 col-sm-4 col-xs-12 col-lg-3">
			<div class="uac_nav_items">
				<li><a class="create_project" href="#create_project">Create a new
						project</a></li>
				<h3>Projects</h3>
				@if(isset($projects[0])) @foreach($projects as $project)
				<li><a class="project_info"
					data-logfile="{{ $project->log_file }}"
					data-project_id="{{ $project->id }}"
					 href="#project_info">{{
						$project->project_name }}</a></li> @endforeach @else
				<p>No projects! Create one to see them here.</p>
				@endif
			</div>
		</div>
		<div class="col-md-8 col-sm-8 col-xs-12 col-lg-9">
			<div class="uac_profile">
				<div class="uac_sidebar_header">{{$user->name}}</div>
				<div class="uac_profile_itmes">
					<div name="create_project" id="create_project">
						<form class="form-inline" action="/createLogProject" method="post">
							{{ csrf_field() }}
							<div class="form-group">
								<label for="project">Create New Project:</label> <input
									type="text" class="form-control" id="project_name"
									name="project_name" placeholder="Name of the project" required>
							</div>
							<button type="submit" class="btn btn-default">Create</button>
						</form>
					</div>
					<div id="project_info" name="project_info" class="hidden">
						<div id="upload_script" name="upload_script" class="hidden"></div>
						<div id="projfiles" name="projfiles" class="hidden"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('pagejquery')
<script>
$(document).ready(function(){
	$(".alert-info").fadeTo(2000, 500).slideUp(500, function(){
        $(".alert-info").slideUp(500);
      });
	$(".alert-danger").fadeTo(2000, 500).slideUp(500, function(){
        $(".alert-danger").slideUp(500);
      });
		});
 	$(".project_info").click(function(){
 		$('#upload_script').html('');
 		$('#projfiles').html('');
    	$("#create_project").addClass('hidden');
        $("#project_info").removeClass('hidden');
        $(".uac_sidebar_header").html($(this).html());
        var project_id = $(this).data('project_id');
        if ($(this).data('logfile') === 0){
        	 $('#upload_script').removeClass('hidden');
           $('#upload_script').html(' <form method="POST" class="form-inline" action="/uploadlogfiles/'+project_id+'" enctype="multipart/form-data"  >\
	        		   {!! csrf_field() !!}\
	        		   <div class="form-group">\
						<label for="logfiles">Log files <span\
						  style="color: #a94442;">*</span> </label> <input type="file"\
							class="form-control" id="logfiles" name="logfiles" required>\
					</div>\
					<button type="submit" class="btn btn-default">Submit</button>\
	   				</form>'
           );
        }
        else{
        	$('#projfiles').removeClass('hidden');
        	$('#projfiles').append('<h2>Project Files :</h2>');
			$.ajax({
			  type: "GET",
			  url: '/getLogProjects',
			  data: {
				  'project_id' : project_id
			  },
			  success: function(data) {
				$.each(data, function( index, value ) {
				  $('#projfiles').append('<a href="#"><span>'+value.doc_name+'</span> </a> <small> '+value.version+'</small><br><br>');
				});
			  
			}
		});
    }
	});
 	$(".create_project").click(function(){
 		$("#project_info").addClass('hidden');
        $("#create_project").removeClass('hidden');
        $(".uac_sidebar_header").html('{{$user->name}}');
 	});
</script>
@endsection
