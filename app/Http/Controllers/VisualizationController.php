<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Projects;
use Redirect;
use App\User;
use Validator;
use File;
use Session;
use App\Documents;

class VisualizationController extends Controller {

	public function index(Request $request) {
		$user_id = Auth::id ();
		$user = User::find ( $user_id );
		$projects = Projects::where ( 'user_id', $user_id )->where ( 'project_type', 'visualize' )->get ();
		return view ( 'user.visualization', compact ( 'user', 'projects' ) );
	}
	public function createProject(Request $request) {
		$user_id = Auth::id ();
		$project = new Projects ();
		$project->user_id = $user_id;
		$project->project_name = $request->get ( 'project_name' );
		$project->project_type = "visualize";
		$project->save ();
		return Redirect::back ();
	}
	public function getProjects(Request $req) {
		$proj_docs = Documents::where ( 'proj_id', $req->project_id )->where('doc_type','logFile')->get ();
		return response ()->json ( $proj_docs );
	}
	public function uploadLogfiles(Request $request, $pid) {
		$rules = array (
				'logfiles' => 'required',
				'logfiles_extension' => 'in:csv,log'
		);
		$messsages = array (
				'logfiles.required' => 'Log file is required.',
				'logfiles_extension.in' => 'Log file must be only of csv, log extension'
		);
		$validator = Validator::make ( [
				'logfiles' => $request->file ( 'logfiles' ),
				'logfiles_extension' => strtolower ( $request->file ( 'logfiles' )->getClientOriginalExtension () )
		], $rules, $messsages );
		if ($validator->fails ())
			return Redirect::back ()->withErrors ( $validator );
			else {
				$user_id = Auth::id ();
				$user = User::find ( $user_id );
				$project = Projects::findOrFail ( $pid );
				if ($request->hasFile ( 'logfiles' )) {
					$file = $request->file ( 'logfiles' );
					$original_name = $file->getClientOriginalName ();
					$extension = $file->getClientOriginalExtension ();
					$mime = $file->getClientMimeType ();
					$filename = $file->getFilename ();
					$document_url = $user->name . '/projects/' . $project->project_name . '/logs' . '/' . $filename . '.' . $extension;
					$document_filename = $original_name;
					$file->move ( $user->name . '/projects/' . $project->project_name . '/logs/', $filename . '.' . $extension );
					$document = new Documents ();
					$document->user_id = $user_id;
					$document->doc_name = $document_filename;
					$document->doc_path = $document_url;
					$document->proj_id = $project->id;
					$project->log_file = 1;
					$document->doc_type = 'logFile';
					$logFile = file ( $document_url );
					$headersLine = $logFile [0];
					$headersLine = str_replace ( "\n", "", $headersLine );
					$headers = explode ( ",", $headersLine );
					$headersPresent = array (
							"timeStamp",
							"elapsed",
							"label",
							"responseCode",
							"responseMessage",
							"threadName",
							"dataType",
							"success",
							"failureMessage",
							"bytes",
							"grpThreads",
							"allThreads",
							"Latency",
							"IdleTime"
					);
					$arr_diff = array_diff ( $headersPresent, $headers );
					if (count ( $arr_diff ) > 0) {
						foreach ( $arr_diff as $missing_header )
							$message_missing_header [] = $missing_header;
						$message = "Missing headers : '" . implode ( ",", $message_missing_header ) . "'. So please check the log file headers an re-upload again.";
						Session::flash ( 'logError-message', $message );
						return Redirect::back ();
					} else {
						$contents = '# Variables : Index Name : Index Name should be the timestamp and the User ID : UserID_timeStamp_testID
# Variable : Path Name : Path Name should be the <Timestamp>_<TestID>.csv
	
input {
    file {
        path => ' . $document_url . '
        start_position => "beginning"
    }
}
	
filter {
	
  if ([message] =~ "responseCode") {
    drop { }
  } else {
    csv {
        columns => ["' . implode ( "\",\"", $headers ) . '"]
    }
    date {
      match => [ "timeStamp", "UNIX_MS" ]
    }
	
    mutate {
      convert => [ "elapsed", "integer" ]
      convert => [ "latency", "integer" ]
      convert => [ "bytes", "integer" ]
	  convert => [ "grpThreads", "integer" ]
	  convert => [ "allThreads", "integer" ]
	  convert => [ "SampleCount", "integer" ]
	  convert => [ "ErrorCount", "integer" ]
	  convert => [ "IdleTime", "integer" ]
	  convert => [ "Connect", "integer" ]
   
    }
  }
}
output {
#  stdout { codec => rubydebug }
  elasticsearch {
    hosts => "127.0.0.1"
    index => "logstash-jmeter-all-%{+YYYY.MM.dd}"
  }
} ';
						File::put ( $user->name . '/projects/' . $project->project_name . '/logs/' . $filename . '.conf', $contents );
						$project->update ();
						$document->save ();
						$document = new Documents ();
						$document->user_id = $user_id;
						$document->doc_name = $filename;
						$document->doc_path = $user->name . '/projects/' . $project->project_name . '/logs' . '/' . $filename . '.conf' ;
						$document->proj_id = $project->id;
						$document->doc_type = 'confFile';
						Session::flash ( 'success-message', 'File(s) uploaded successfully.' );
						return Redirect::back ();
					}
				}
				
			}
	}
}
