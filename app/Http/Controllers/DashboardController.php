<?php

namespace App\Http\Controllers;

use App\User;
use Auth;
use App\Documents;
use Illuminate\Http\Request;
use Redirect;
use Session;
use Validator;
use App\Projects;
use File;

class DashboardController extends Controller {
	public function dashboard() {
		$user_id = Auth::id ();
		$user = User::find ( $user_id );
		$projects = Projects::where ( 'user_id', $user_id )->where ( 'project_type', 'testing' )->get ();
		foreach ( $projects as $project ) {
			$documents = Documents::where ( 'proj_id', $project->id )->get ();
			if (isset ( $documents [0] )) {
				$docdata [] = $documents;
			}
		}
		return view ( 'user.dashboard', compact ( 'user', 'projects', 'docdata' ) );
	}
	public function getProjects(Request $req) {
		$proj_docs = Documents::where ( 'proj_id', $req->project_id )->where ( 'doc_type', '!=', 'testing' )->get ();
		return response ()->json ( $proj_docs );
	}
	public function createProject(Request $request) {
		$user_id = Auth::id ();
		$project = new Projects ();
		$project->user_id = $user_id;
		$project->project_name = $request->get ( 'project_name' );
		$project->project_type = "testing";
		$project->save ();
		return Redirect::back ();
	}
	public function uploadDatafiles(Request $request, $pid) {
		$rules = array (
				'datafiles' => 'required',
				'datafiles_extension' => 'in:xls,csv,xlsx,log,txt' 
		);
		$messsages = array (
				'datafiles.required' => 'Data file is required.',
				'datafiles_extension.in' => 'Data file must be only of xls, csv, xlsx, log, txt extension' 
		);
		$validator = Validator::make ( [ 
				'datafiles' => $request->file ( 'datafiles' ),
				'datafiles_extension' => strtolower ( $request->file ( 'datafiles' )->getClientOriginalExtension () ) 
		], $rules, $messsages );
		if ($validator->fails ())
			return Redirect::back ()->withErrors ( $validator );
		else {
			$user_id = Auth::id ();
			$user = User::find ( $user_id );
			$project = Projects::findOrFail ( $pid );
			if ($request->hasFile ( 'datafiles' )) {
				$file = $request->file ( 'datafiles' );
				$original_name = $file->getClientOriginalName ();
				$extension = $file->getClientOriginalExtension ();
				$mime = $file->getClientMimeType ();
				$filename = $file->getFilename ();
				$document_url = $user->name . '/projects/' . $project->project_name . '/docs' . '/' . $filename . '.' . $extension;
				$document_filename = $original_name;
				$file->move ( $user->name . '/projects/' . $project->project_name . '/docs/', $filename . '.' . $extension );
				$document = new Documents ();
				$document->user_id = $user_id;
				$document->doc_name = $document_filename;
				$document->doc_path = $document_url;
				$document->proj_id = $project->id;
				$document->doc_type = 'dataFile';
				$project->datafiles = 1;
				$project->update ();
				$document->save ();
			}
			Session::flash ( 'success-message', 'File(s) uploaded successfully.' );
			return Redirect::back ();
		}
	}
	public function uploadJmx(Request $request, $pid) {
		$rules = array (
				'myfiles' => 'required',
				'jmx_extension' => 'in:jmx' 
		);
		$messsages = array (
				'myfiles.required' => 'Jmx script file is required.',
				'jmx_extension.in' => 'Jmx script file must be a .jmx extension' 
		);
		$validator = Validator::make ( [ 
				'myfiles' => $request->file ( 'myfiles' ),
				'jmx_extension' => strtolower ( $request->file ( 'myfiles' )->getClientOriginalExtension () ) 
		], $rules, $messsages );
		if ($validator->fails ())
			return Redirect::back ()->withErrors ( $validator );
		else {
			$user_id = Auth::id ();
			$user = User::find ( $user_id );
			$project = Projects::findOrFail ( $pid );
			if ($request->hasFile ( 'myfiles' )) {
				$file = $request->file ( 'myfiles' );
				$original_name = $file->getClientOriginalName ();
				$extension = $file->getClientOriginalExtension ();
				$mime = $file->getClientMimeType ();
				$filename = $file->getFilename ();
				$document_url = $user->name . '/projects/' . $project->project_name . '/docs' . '/' . $filename . '.' . $extension;
				$document_filename = $original_name;
				$file->move ( $user->name . '/projects/' . $project->project_name . '/docs/', $filename . '.' . $extension );
				$document = new Documents ();
				$document->user_id = $user_id;
				$document->doc_name = $document_filename;
				$document->doc_path = $document_url;
				$xml = new \SimpleXMLElement ( $document_url, null, true );
				$document->version = $xml ['jmeter'];
				$document->proj_id = $project->id;
				$project->jmx_script_file = 1;
				$document->doc_type = 'jmxScript';
				$project->update ();
				$document->save ();
			}
			Session::flash ( 'success-message', 'File(s) uploaded successfully.' );
			return Redirect::back ();
		}
	}
}
