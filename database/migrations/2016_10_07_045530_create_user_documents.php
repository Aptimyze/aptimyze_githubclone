<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class CreateUserDocuments extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create ( 'documents', function (Blueprint $table) {
			$table->increments ( 'id' );
			$table->integer ( 'user_id' )->foreign ( 'user_id' )->references ( 'id' )->on ( 'users' )->nullable ();
			$table->string ( 'doc_name' );
			$table->string ( 'doc_path' );
			$table->string ( 'doc_type' );
			$table->string ( 'proj_id' );
			$table->string ( 'version' );
			$table->timestamps ();
		} );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::drop ( 'documents' );
	}
}
